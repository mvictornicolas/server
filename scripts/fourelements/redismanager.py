import redis
from json import load
from tools.fourelements.configuration.get_workstation_ids import get_workstation_name_and_ids

with open('./configuration/redis.json') as json_file:
    data = load(json_file)
prod_configs = data['prod']['configs']
qa_configs = data['qa']['configs']
conf_database = load(open("./configuration/database.json"))
# r = redis.Redis(
#     host=configs['host'],
#     port=configs['port'],
#     password=configs['password']
#     )

def redis_manage(keys, server):
    servers = {
        'prod': prod_configs,
        'qa': qa_configs
    }
    rd = redis.Redis(
        host=servers[server]['host'],
        port=servers[server]['port'],
        password=servers[server]['password']
        )
    try:
        if keys['key'] == '#wsgetid':
            res = get_ws_id(keys['cmdInput'].upper())
            print(res)
            messages = [f'Esse é o ID do posto {keys["cmdInput"]}:','', res, res.upper().replace('-','')]
            return {"aessia": messages}
        elif keys['key'] == '#wsgetstatus':
            res = get_ws_status(keys['cmdInput'].upper(), rd)
            print(res)
            messages = [res]
            return {"aessia": messages}
        elif keys['key'] == '#wsgetbusy':
            res = get_busy_wss(rd)
            print(res)
            messages = [res]
            return {"aessia": messages}
        elif keys['key'] == '#wssetavailable':
            res = set_available_wss(keys['cmdInput'].upper().split(), rd)
            print(res)
            messages = ['As workstations']
            for w in keys['cmdInput'].upper().split():
                messages.append(w)
            messages.append('Foram liberadas com sucesso')
            return {"aessia": messages}
        elif keys['key'] == '#wssetbusy':
            res = set_busy_wss(keys['cmdInput'].upper().split(), rd)
            print(res)
            messages = ['As workstations']
            for w in keys['cmdInput'].upper().split():
                messages.append(w)
            messages.append('Foram bloqueadas com sucesso')
            return {"aessia": messages}
    except KeyError:
        return {"aessia":['Perdao, identifiquei algo invalido em sua solicitacao']}

def get_ws_id_list(ws_name_list=[]):
    ws_ids = get_workstation_name_and_ids(conf_database)
    try:
        ws_list = []
        for ws in ws_name_list:
            ws_id = ws_ids[ws]
            ws_list.append(ws + ': ' + ws_id)
        return ws_list
    except:
        print('Error getwsid')
        return 'Nao encontrei nenhuma workstation nomeada assim'+'[RM0]'

def get_ws_id(ws_name):
    ws_ids = get_workstation_name_and_ids(conf_database)
    ws_id = ws_ids[ws_name]
    return ws_id

def get_ws_status(ws_name, rd):
    try:
        ws_id = get_ws_id(ws_name)
    except KeyError:
        return 'Nao encontrei nenhuma workstation nomeada assim'+'[RM2]'
    redis_response = rd.hget(f'sessions:{ws_id}', 'connected')
    if '1' in str(redis_response):
        redis_response = "Em uso"
    elif '0' in str(redis_response):
        redis_response = "Livre"
    else:
        return 'Nao encontrei nenhuma workstation nomeada assim'+'[RM1]'
    return ws_name+' '+redis_response

def get_busy_wss(rd):
    ws_ids = get_workstation_name_and_ids(conf_database)
    busy = False
    wss_busy = ['As workstations ocupadas sao: ']
    for w in ws_ids:
        redis_command = rd.hget(f'sessions:{ws_ids[w]}', 'connected')
        if '1' in str(redis_command):
            busy = True
            wss_busy.append(w)
    if busy == True:
        message = wss_busy[0]
        if len(wss_busy) > 2:
            for i in wss_busy[1:]:
                message = message+i+', '
            message = message[0:len(message)-2]
        else:
            for i in wss_busy:
                message+=i
    elif busy == False:
        message = "Todas as workstations estao livres"
    else:
        message = 'Unexpected Error Occurred'
    return message

def set_available_wss(ws_name_list, rd):
    ws_list = []
    ws_response_list = []
    for w in ws_name_list:
        wid = get_ws_id(w)
        ws_list.append(wid)
    for wsid in ws_list:
        redis_response = rd.hset(f'sessions:{wsid}', 'connected', '0')
        ws_response_list.append(wsid+': '+str(redis_response))
    return ws_response_list

def set_busy_wss(ws_name_list, rd):
    ws_list = []
    ws_response_list = []
    for w in ws_name_list:
        wid = get_ws_id(w)
        ws_list.append(wid)
    for wsid in ws_list:
        redis_response = rd.hset(f'sessions:{wsid}', 'connected', '1')
        ws_response_list.append(wsid+': '+str(redis_response))
    return ws_response_list
