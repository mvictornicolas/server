import requests


def request_devices(hostname, port):
    response = requests.get(f'http://{hostname}:{port}/api/devices')
    print(response.text)
    return response
