from tools.generic.timeweaver import get_datetime_now
from tools.generic.panda.read_sql import read_sql
from tools.generic.idweaver import upperfy_id
from tools.generic.idweaver import lowerfy_id
import cx_Oracle


def get_label_by_serial_number(conf_database, serial_number):
    conf = conf_database['oracle']
    dsn = cx_Oracle.makedsn(
        conf['hostname'],
        conf['portnumber'],
        service_name=conf['servicename']
    )
    try:
        cx_Oracle.init_oracle_client(lib_dir='./instantclient_19_9')
    except cx_Oracle.DatabaseError:
        pass
    conn = cx_Oracle.Connection(user='TNT_OPERATIONAL', password=conf['psw'], dsn=dsn)      
    exportdate = get_datetime_now()
    query = f"SELECT LABEL FROM TNT_OPERATIONAL.PRODUCT WHERE SERIAL_NUMBER = '{serial_number}'"
    data = read_sql(query, conn)
    print(data)
    try:
        final_label = data['LABEL'].iloc[0]
    except IndexError as exc:
        print(exc)
        return None
    return final_label

def get_serial_number_by_workstation(conf_database, workstation):
    pass

def get_running_operation_by_serial_number(conf_database, serial_number):
    pass
