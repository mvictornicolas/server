import json
#from urllib.parse import quote_plus
from scripts.fourelements.componentnfsolve import solve_component_not_found
from tools.fourelements.requesting.req_token import get_auth
from tools.fourelements.requesting.update_workplan_request import get_update_workplan_data
from tools.fourelements.workplans.get_workplan_num import get_workline_by_num_workplan
from tools.fourelements.requesting.req_workplan_query import workplan_query
from tools.generic.idweaver import lowerfy_id

def workplan_component_not_found_error(data):
    hostname = data['hostname']
    num_workplan = data['numWorkplan']
    print(num_workplan)
    headers = get_auth(f"http://{hostname}:8087/api/users/login")
    conf_database = json.load(open("./configuration/database.json"))
    workline_id = get_workline_by_num_workplan(num_workplan, conf_database)
    conf_payload = get_update_workplan_data(hostname, num_workplan, lowerfy_id(workline_id))
    workplan_id = workplan_query(f"http://{hostname}:8085/api/workplans?size=30&page=0&filter={num_workplan}", headers, lowerfy_id(workline_id))['content'][0]['id']
    response = solve_component_not_found(workplan_id, headers, conf_payload, conf_database)
    print(response)
    return response
