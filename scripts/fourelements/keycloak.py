import requests
import json


def get_keycloak_token(data):
    with open("./configuration/keycloak.json") as json_file:
        keycloak_config = json.load(json_file)
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    if data['server'] == 'qa':
        address = keycloak_config['qa']['address']
        user = keycloak_config['qa']['user']
        psw = keycloak_config['qa']['psw']
    elif data['server'] == 'prod':
        address = keycloak_config['prod']['address']
        user = keycloak_config['prod']['user']
        psw = keycloak_config['prod']['psw']
    params = {
        'username': user,
        'password': psw,
        'grant_type': 'password',
        'client_id': 'admin-cli'
    }
    response = requests.post(f'http://{address}/auth/realms/master/protocol/openid-connect/token', headers=headers, data=params)
    print('*'*20)
    print(response.text)
    print('*'*20)
    return response.json()['access_token']


def create_keycloak_user(data, token):
    try:
        with open("./configuration/keycloak.json") as json_file:
            keycloak_config = json.load(json_file)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        params = {
            'realm': 'master',
        }
        user_data = {
            'username': data['badge'],
            'firstName': data['firstName'],
            'lastName': data['lastName'],
            'enabled': 'true',
            'credentials': [{
                'type': 'password',
                'value': data['badge'],
                'temporary': False
            }],
            'attributes': {
                'BADGE': data['badge'],
                'default-language': 'pt',
                'jobPosition': data['role'],
                'worklines': data['worklines']
            }
        }
        if data['server'] == 'qa':
            address = keycloak_config['qa']['address']
        elif data['server'] == 'prod':
            address = keycloak_config['prod']['address']
        response = requests.post(f'http://{address}/auth/admin/realms/as2group/users', headers=headers, json=user_data, params=params)
        print('*'*20)
        print(response.text)
        print('*'*20)
    except Exception as exc:
        print('Error creating user: ' + exc)
        return {'error': exc}
    return response.text
