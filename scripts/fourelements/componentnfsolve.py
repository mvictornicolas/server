import re, requests, datetime, cx_Oracle, uuid
import pandas as pd

from tools.generic.connectors.oracle import oracle_connect
from tools.generic.timeweaver import get_datetime_now
from tools.generic.idweaver import upperfy_id
from tools.generic.panda.read_sql import read_sql
from tools.generic.panda.convert_list_query import to_query
from tools.generic.panda.sql import create_select, create_insert

def solve_component_not_found(workplan_id, headers, payload, conf_database, hostname='172.17.1.192', port='8085'):
    f = payload
    url = f"http://{hostname}:{port}/api/workplans/{workplan_id}"
    response = requests.request("PUT", url, headers=headers, data=str(payload))
    print(response.text)
    bad_ids = []
    big_bad_ids = []
    bad_ids_txt = ""

    if 'component not found' in response.text:
        bid = response.json()["value"]
        bad_ids.append(bid)
        big_bad_ids.append(bid.upper().replace('-', ''))
        bad_ids_txt = bad_ids_txt+f"'{bid}', \n"
        print(bid)
        component_error = True
        while component_error == True:
            regex = r'{\n *"id": "'+bid+r'.*'+bid+r'"\n *}[,]{0,1}'
            f = re.sub(regex, '', f, flags=re.DOTALL)
            response = requests.request("PUT", url, headers=headers, data=f)
            if 'component not found' in response.text:
                bid = response.json()["value"]
                bad_ids.append(bid)
                big_bad_ids.append(bid.upper().replace('-',''))
                bad_ids_txt = bad_ids_txt+f"'{bid}', \n"
            else:
                component_error = False
                response = requests.request("PUT", url, headers=headers, data=f)
            print(bid)
    print(bad_ids)
    if len(bad_ids) == 0:
        return {'aessia':'Nenhum erro de componente encontrado'}
    conn = oracle_connect(conf_database)
    exportdate = get_datetime_now()
    query = create_select('TNT_OPERATIONAL.WORKPLAN', 'ID', upperfy_id(workplan_id))
    data = read_sql(query, conn)
    num_workplan = data['NUM_WORKPLAN'][0]
    print(num_workplan)

    query = create_select('TNT_OPERATIONAL.WORKPLAN_OPERATION', 'WORKPLAN_ID', upperfy_id(workplan_id))
    data = read_sql(query, conn)

    query_list = str(to_query(data['ID'])).replace('[','').replace(']','')
    big_bad_ids = str(to_query(big_bad_ids, pandas=False)).replace('[','').replace(']','')

    print(query_list,big_bad_ids)
    query = f"SELECT * FROM TNT_OPERATIONAL.WORKPLAN_OPERATION_COMPONENT WHERE WORKPLAN_OPERATION_ID IN ({query_list}) AND COMPONENT_ID IN ({big_bad_ids})"
    data = read_sql(query, conn)

    sql_commands=create_insert(
        data, 
        'TNT_OPERATIONAL.WORKPLAN_OPERATION_COMPONENT',
        ['ID', 'QTY_DESIRED', 'WORKPLAN_OPERATION_ID', 'COMPONENT_ID'],
        ['ID', 'QTY_DESIRED', 'WORKPLAN_OPERATION_ID', 'COMPONENT_ID']
    )

    file = open(f'bad_ids_from {num_workplan} DATE {exportdate} ID {upperfy_id(workplan_id)}.txt', 'w')
    file.write(
        num_workplan+'\n'+
        exportdate+'\n'+
        f'ID: {upperfy_id(workplan_id)} or {workplan_id}'+'\n\n'+
        'BAD COMPONENT IDS: '+'\n'+bad_ids_txt+'\n\n'+
        'This is the query to find the bad boys:\n'+query+'\n\n'+
        'This is the backup data from the bad boys:\n'+sql_commands+'\n'
    )
    file.close()

    response = {"aessia":
        num_workplan+'\n'+
        exportdate+'\n'+
        f'ID: {upperfy_id(workplan_id)} or {workplan_id}'+'\n\n'+
        'BAD COMPONENT IDS: '+'\n'+bad_ids_txt+'\n\n'+
        'Essa é a query para encontrar os bad boys:\n'+query+'\n\n'+
        'Esse é o insert para reintegrar os bad boys:\n'+sql_commands+'\n'
    }
    print('Closing connection')
    conn.close()
    return response