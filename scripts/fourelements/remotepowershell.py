import subprocess, re
from json import load
def view_ram_modules_usage(ipaddress):
    comm_failure = "Falha ao comunicar com o servidor..."
    comm_success = "Estado de uso de RAM dos modulos do 4Elements: "
    try:
        response = str(subprocess.check_output(['''powershell.exe''', '''$password = ConvertTo-SecureString “Navistar1” -AsPlainText -Force; $Cred = New-Object System.Management.Automation.PSCredential (“international\\t00vnm”, $password); Get-WmiObject Win32_Process -ComputerName '''+ipaddress+''' -Credential $Cred -Filter "name = 'java.exe'" | Sort-Object -Descending WS | Select WS, CommandLine''']).decode('ASCII')).strip().lower()
    except:
        return [comm_failure]
    doc = [comm_success,'']
    for line in response.split('\n'):
        if '"java"' in line.lower():
            if '.jar' not in line:
                continue
            line = line.replace(r'\r', '').strip().split(".jar")[0]+'.jar'
            last_bar = line.rfind('\\')
            name = line[last_bar:].replace('\\','').capitalize()
            number = re.findall('[\d]{1,11}', line)[0]
            if len(number) == 10:
                number = number[0]+','+number[1]+'GB RAM'
            elif len(number) == 9:
                number = number[0:3]+'MB RAM'
            line = f'{name} - {number}'
            doc.append(line)
    print(doc)
    return doc
