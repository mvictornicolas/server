#TODO create an integration API test(to let the group users see if the OPC problems are related to 4ELEMENTS or don't)
from requests import post
from json import loads

def test_integration(ipaddress):
    headers = {'Content-Type':'application/json',}
    data = '{ "client_id": "tnt", "username": "aessia", "password": "aessia" }'
    try:
        response = post(f'http://{ipaddress}:8081/api/v1/users/authenticate', headers=headers, data=data)
    except:
        return {"aessia":"Falha ao comunicar com o 4Elements via Integration"}
    response = loads(response.text)
    access_token = response['authenticationInfo']['access_token']
    headers = {'Content-Type': 'application/json','workline-context': '625b7d8c-b610-4783-962c-25965edb2446','Authorization': f'Bearer {access_token}'}
    data = '{ "config": "JUST_SCAN", "sourcePin": "766S00014041620KXX558332" }'
    try:
        response = post(f'http://{ipaddress}:8081/api/v1/products/scan', headers=headers, data=data)
    except:
        return {"aessia":"Falha ao comunicar com o 4Elements via Integration"}
    response = loads(response.text)
    response['aessia'] = "Sucesso ao comunicar com o 4Elements via Integration"
    return response
