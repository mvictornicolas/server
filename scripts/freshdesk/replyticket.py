from requests import post
def reply_ticket(ticket_id, body):
    headers = {'Content-Type': 'application/json'}
    data = '{"body":"'+body+'"}'
    response = post(f'https://as2group.freshdesk.com/api/v2/tickets/{ticket_id}/reply', headers=headers, data=data, auth=('suporte@as2group.com.br', 'usuario*feliz'))
    return response
    