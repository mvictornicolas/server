from requests import post
def note_ticket(ticket_id, body, private=False):
    if private == False:
        private = "false"
    elif private == True:
        private = "true"
    headers = {'Content-Type': 'application/json'}
    data = '{"body": "'+body+'", "private": '+private+'}'
    response = post(f'https://as2group.freshdesk.com/api/v2/tickets/{ticket_id}/notes', headers=headers, data=data, auth=('suporte@as2group.com.br', 'usuario*feliz'))
    return response
