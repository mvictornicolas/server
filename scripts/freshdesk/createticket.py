from requests import post
def create_ticket(subject='Auto-Generated Ticket', group_id='47000651461', email='autoticket@as2group.com.br', description='Auto-Generated Ticket', source='2', priority='1', status='2', responder_id='47017012166', type='Tenho um problema'):
    files = {
        'email': (None, email),
        'group_id': (None, group_id),
        'subject': (None, subject),
        'description': (None, description),
        'source': (None, source),
        'priority': (None, priority),
        'status': (None, status),
        'responder_id': (None, responder_id),
        'type': (None, type),
    }
    response = post('https://as2group.freshdesk.com/api/v2/tickets', files=files, auth=('suporte@as2group.com.br', 'as2Group@'))
    return response
