def validate_json(data, fields):
    validated_data = []
    invalidated_data = []
    for f in fields:
        try:
            validated_data.append(data[f])
        except (ValueError, TypeError, KeyError):
            invalidated_data.append(f)
    return {"validatedData":validated_data, "invalidatedData":invalidated_data}
