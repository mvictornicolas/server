
def validate_json(data, fields):
    validated_data = {}
    invalidated_data = {}
    for f in fields:
        try:
            validated_data[f] = data[f]
        except (ValueError, TypeError, KeyError):
            invalidated_data[f] = f
    return {"validatedData":validated_data, "invalidatedData":invalidated_data}