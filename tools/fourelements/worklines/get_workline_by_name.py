from tools.generic.idweaver import lowerfy_id
from tools.generic.panda.read_sql import read_sql
import cx_Oracle


def get_workline_id_by_name(workline, server, conf_database):
    conf_database['oracle']['username'] = 'TNT_CONFIGURATION'
    conf_database['oracle_qa']['username'] = 'TNT_CONFIGURATION'
    if server == 'qa':
        conf = conf_database['oracle_qa']
    elif server == 'prod':
        conf = conf_database['oracle']
    else:
        raise "bad server error"
    dsn = cx_Oracle.makedsn(
        conf['hostname'],
        conf['portnumber'],
        service_name=conf['servicename']
    )
    try:
        cx_Oracle.init_oracle_client(lib_dir='./instantclient_19_9')
    except cx_Oracle.DatabaseError:
        pass
    conn = cx_Oracle.Connection(user=conf['username'], password=conf['psw'], dsn=dsn)
    query = f"SELECT ID FROM TNT_CONFIGURATION.WORKLINE WHERE EXTERNAL_ID = '{str(workline.upper())}'"
    data = read_sql(query, conn)
    try:
        workline_id = lowerfy_id(data['ID'][0])
    except IndexError:
        workline_id = ''
    print('Closing connection')
    conn.close()
    print('this is the workline got: ' + workline_id)
    return workline_id
