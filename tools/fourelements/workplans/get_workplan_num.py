# from tools.generic.connectors.oracle import oracle_connect
from tools.generic.timeweaver import get_datetime_now
from tools.generic.panda.read_sql import read_sql
from tools.generic.panda.sql import create_select
# from tools.generic.idweaver import upperfy_id
import cx_Oracle


def get_workline_by_num_workplan(num_workplan, conf_database):
    conf = conf_database['oracle']
    dsn = cx_Oracle.makedsn(
        conf['hostname'],
        conf['portnumber'],
        service_name=conf['servicename']
    )
    try:
        cx_Oracle.init_oracle_client(lib_dir='./instantclient_19_9')
    except cx_Oracle.DatabaseError:
        pass
    conn = cx_Oracle.Connection(user=conf['username'], password=conf['psw'], dsn=dsn)
    exportdate = get_datetime_now()
    query = create_select('TNT_OPERATIONAL.WORKPLAN', 'NUM_WORKPLAN', num_workplan, final="ORDER BY REL DESC")
    data = read_sql(query, conn)
    workstation_id = data['WORKSTATION_ID'][0]
    conf_database['oracle']['username'] = 'TNT_CONFIGURATION'
    conn.close()
    conn = cx_Oracle.Connection(user=conf['username'], password=conf['psw'], dsn=dsn)
    query = create_select('TNT_CONFIGURATION.WORKSTATION', 'ID', workstation_id)
    data = read_sql(query, conn)
    workline_id = data['WORKLINE_ID'][0]
    print('Closing connection')
    conn.close()
    print('this is the workline got: ' + workline_id)
    return workline_id
