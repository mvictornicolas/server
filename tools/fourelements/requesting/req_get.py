from requests import request
from json import loads
def generic_get(url, headers):
    response = request("GET", url, headers=headers, data={})
    return loads(response.text)