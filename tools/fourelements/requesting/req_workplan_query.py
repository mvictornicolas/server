from requests import request
from json import loads
def workplan_query(url, headers, workline):
    headers['workline-context'] = workline
    response = request("GET", url, headers=headers, data={})
    return loads(response.text)
