import requests, json
from tools.fourelements.requesting.req_token import get_auth
from tools.fourelements.requesting.req_get import generic_get
from tools.fourelements.requesting.req_workplan_query import workplan_query

def get_update_workplan_data(hostname, filter, workline):
    headers = get_auth(f"http://{hostname}:8087/api/users/login")
    workplan_data = workplan_query(f"http://{hostname}:8085/api/workplans?size=30&page=0&filter={filter}", headers, workline)['content'][0]    

    workplan_num = workplan_data["number"]
    workplan_id = workplan_data["id"]
    workstation_id = workplan_data["workstationId"]

    final_response = generic_get(f"http://{hostname}:8085/api/workplans/{workplan_id}", headers)
    workstation_response = generic_get(f"http://{hostname}:8083/api/workstations/{workstation_id}", headers)
    part_response = generic_get(f"http://{hostname}:8082/api/parts/{final_response['partId']}?revision={final_response['partRelease']}", headers)

    full_request = {
        "cycleTime": final_response["cycleTime"],
        "description": final_response["description"],
        "draft": final_response["draft"],
        "id": final_response["id"],
        "justification": {
            "documentTypeId": "06ed938b-1c40-4fb7-ba69-6a60eddfd5fa",
            "documentNumber": "AAA",
            "description": "AAA"
        },
        "partId": final_response["partId"],
        "partRelease": final_response["partRelease"],
        "workplanNumber": final_response["workplanNumber"],
        "workstationId": final_response["workstationId"],
        "number": final_response["number"],
        "release": final_response["release"],
        "status": final_response["status"],
        "actualUser": final_response["actualUser"],
        "actualAt": final_response["actualAt"],
        "effectiveAt": final_response["effectiveAt"],
        "workstation": workstation_response,
        "part": part_response
    }

    full_request["operations"] = []
    counter = 0
    for o in final_response["operations"]:
        operation = {
            "id": final_response["operations"][counter]["id"],
            "minorSequence": final_response["operations"][counter]["minorSequence"],
            "majorSequence": final_response["operations"][counter]["majorSequence"],
            "operationId": final_response["operations"][counter]["operationId"],
            "galleryInfoId": final_response["operations"][counter]["galleryInfoId"],
            "devices": final_response["operations"][counter]["devices"],
            "helpMarkers": final_response["operations"][counter]["helpMarkers"],
            "ordination": counter,
            "operation": {
                "id": final_response["operations"][counter]["operation"]["id"],
                "description": final_response["operations"][counter]["operation"]["description"],
                "number": final_response["operations"][counter]["operation"]["number"],
                "release": final_response["operations"][counter]["operation"]["release"],
                "classificationCriteria": final_response["operations"][counter]["operation"]["classificationCriteria"],
                "cycleTime": final_response["operations"][counter]["operation"]["cycleTime"],
                "ordination": counter,
                "function": final_response["operations"][counter]["operation"]["function"],
                "functionLabel": final_response["operations"][counter]["operation"]["function"]["label"],
                "image": None,
                "selected": True
            },
        }
        full_request["operations"].append(operation)
        full_request["operations"][counter]["components"] = []
        counter_b = 0
        for c in final_response["operations"][counter]["components"]:
            component_id = c["id"]
            url = f'http://172.17.1.192:8082/api/components?hierarchical=false&ids={component_id}'
            response = requests.request("GET", url, headers=headers, data={})
            component_response = json.loads(response.text)
            try:
                component = {
                    "id": final_response["operations"][counter]["components"][counter_b]["id"],
                    "partnumber": component_response[0]["partnumber"],
                    "sequence": component_response[0]["sequence"],
                    "description": component_response[0]["description"],
                    "quantity": component_response[0]["quantity"],
                    "measurementUnit": component_response[0]["measurementUnit"],
                    "category": component_response[0]["category"],
                    "effectiveAt": component_response[0]["effectiveAt"],
                    "expirationDate": component_response[0]["expirationDate"],
                    "components": component_response[0]["components"],
                    "componentInfo": component_response[0]["componentInfo"],
                    "revision": component_response[0]["revision"],
                    "componentType": component_response[0]["componentType"],
                    "isConsumableStockable": component_response[0]["isConsumableStockable"],
                    "quantityDesired": final_response["components"][counter_b]["quantityDesired"],
                    "points": final_response["components"][counter_b]["points"],
                    "componentId": final_response["components"][counter_b]["componentId"]
                }
                full_request["operations"][counter]["components"].append(component)
            except IndexError:
                print('No components...')
                component = {}
            counter_b+=1
        counter+=1
    file = open("fullrequest.json", "w")
    file.write(str(json.dumps(full_request, indent=4)))
    file.close()
    return full_request
