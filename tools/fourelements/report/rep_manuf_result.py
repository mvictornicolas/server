import cx_Oracle
import uuid
import json
import datetime
import os
import re
import pandas as pd


def nested_get(dic, keys):
    for key in keys:
        try:
            dic = dic[key]
        except Exception:
            return dic
    return dic


def manufacturing_result_report(hostname, portnumber, servicename, user_name, pw, folder,
                                start_date, end_date, operation_number, data_type, get_serial_number=False, json_fields={}):
    try:
        cx_Oracle.init_oracle_client(lib_dir=os.path.join(os.getcwd(), folder))
    except Exception:
        pass
    dsn_tns = cx_Oracle.makedsn(hostname, portnumber, service_name=servicename)
    conn = cx_Oracle.connect(user=user_name, password=pw, dsn=dsn_tns)
    exportdate = datetime.datetime.now().strftime("%d-%m-%Y %H-%M-%S")
    sqldate = datetime.datetime.now() + datetime.timedelta(hours=3)
    sqldate = format(sqldate, "%Y-%m-%d %H:%M:%S")
    path = os.getcwd()
    path = f'./DATA-{exportdate}'
    os.mkdir(path)
    operation_query = f"SELECT ID FROM TNT_OPERATIONAL.OPERATION WHERE NUM = {operation_number} ORDER BY ACTUAL_AT DESC".replace(r'\n', '')
    operations = pd.read_sql(operation_query, conn)
    try:
        operations['ID'] = operations['ID'].apply(lambda x: str(uuid.UUID(bytes=x)).replace('-', '').upper())
    except Exception:
        pass
    operation_ids = operations['ID'].tolist()
    manufacturing_operation_query = f"SELECT ID FROM TNT_OPERATIONAL.MANUFACTURING_OPERATION WHERE OPERATION_ID IN ({str(operation_ids).replace('[','').replace(']','')}) AND START_AT > TO_DATE('{start_date}', 'YYYY-mm-dd HH24:MI:SS') AND START_AT < TO_DATE('{end_date}', 'YYYY-mm-dd HH24:MI:SS') ORDER BY START_AT DESC".replace(r'\n', '')
    print(manufacturing_operation_query)
    manufacturing_operations = pd.read_sql(manufacturing_operation_query, conn)
    try:
        manufacturing_operations['ID'] = manufacturing_operations['ID'].apply(lambda x: str(uuid.UUID(bytes=x)).replace('-', '').upper())
    except Exception:
        pass
    manufacturing_operation_ids = manufacturing_operations['ID'].tolist()
    mo_chunks = [manufacturing_operation_ids[x:x+1000] for x in range(0, len(manufacturing_operation_ids), 1000)]
    result_count = 0
    if len(manufacturing_operation_ids) <= 0:
        return {"error": "no manufacturing operation ids found"}
    for mo_ids in mo_chunks:
        qry_count = -1
        for mo_id in mo_ids:
            mo_id = "'"+re.sub('\n', '', mo_id)+"',"
            qry_count += 1
            if qry_count == len(mo_ids):
                mo_id = "'"+mo_id+"'"
        if get_serial_number is True:
            if data_type.upper() == 'EFFECTIVE_DATA':
                result_query = f"SELECT p.SERIAL_NUMBER, mr.CREATED_AT, mr.EFFECTIVE_DATA FROM TNT_OPERATIONAL.MANUFACTURING_RESULT mr JOIN TNT_OPERATIONAL.MANUFACTURING_OPERATION mo ON mr.MANUFACTURING_OPERATION_ID = mo.ID JOIN TNT_OPERATIONAL.PRODUCT_MANUFACTURING pm ON pm.ID = mo.PRODUCT_MANUFACTURING_ID JOIN TNT_OPERATIONAL.PRODUCT p ON pm.PRODUCT_ID = p.ID WHERE mr.MANUFACTURING_OPERATION_ID IN ({str(mo_ids).replace('[', '').replace(']', '')}) ORDER BY mr.CREATED_AT DESC".replace(r'\n', '')
            elif data_type.upper() == 'PLAIN_DATA':
                result_query = f"SELECT p.SERIAL_NUMBER, mr.CREATED_AT, mr.PLAIN_DATA FROM TNT_OPERATIONAL.MANUFACTURING_RESULT mr JOIN TNT_OPERATIONAL.MANUFACTURING_OPERATION mo ON mr.MANUFACTURING_OPERATION_ID = mo.ID JOIN TNT_OPERATIONAL.PRODUCT_MANUFACTURING pm ON pm.ID = mo.PRODUCT_MANUFACTURING_ID JOIN TNT_OPERATIONAL.PRODUCT p ON pm.PRODUCT_ID = p.ID WHERE mr.MANUFACTURING_OPERATION_ID IN ({str(mo_ids).replace('[', '').replace(']', '')}) ORDER BY mr.CREATED_AT DESC".replace(r'\n', '')
        elif get_serial_number is False:
            if data_type.upper() == 'EFFECTIVE_DATA':
                result_query = f"SELECT MANUFACTURING_OPERATION_ID, mr.CREATED_AT, mr.EFFECTIVE_DATA FROM TNT_OPERATIONAL.MANUFACTURING_RESULT mr WHERE MANUFACTURING_OPERATION_ID IN ({str(mo_ids).replace('[','').replace(']','')}) ORDER BY CREATED_AT DESC".replace(r'\n', '')
            elif data_type.upper() == 'PLAIN_DATA':
                result_query = f"SELECT MANUFACTURING_OPERATION_ID, mr.CREATED_AT, mr.PLAIN_DATA FROM TNT_OPERATIONAL.MANUFACTURING_RESULT mr WHERE MANUFACTURING_OPERATION_ID IN ({str(mo_ids).replace('[','').replace(']','')}) ORDER BY CREATED_AT DESC".replace(r'\n', '')
        result_data = pd.read_sql(result_query, conn)
        try:
            result_data['MANUFACTURING_OPERATION_ID'] = result_data['MANUFACTURING_OPERATION_ID'].apply(lambda x: str(uuid.UUID(bytes=x)).replace('-', '').upper())
        except Exception:
            pass
        if result_count == 0:
            final_result_dataframe = result_data
        else:
            final_result_dataframe = final_result_dataframe.append(result_data)
        result_count += 1
        print(final_result_dataframe)
    if json_fields != {}:
        for json_field in json_fields:
            final_result_dataframe[json_field] = final_result_dataframe[data_type.upper()].apply(
                lambda x: nested_get(json.loads(str(x)), json_fields[json_field])
                )
    final_result_dataframe.to_excel(f'./DATA-{exportdate}/EXPORTED-RESULTS.xlsx')
    return [f'./DATA-{exportdate}', 'EXPORTED-RESULTS.xlsx']
