from tools.generic.connectors.oracle import oracle_connect
from tools.generic.timeweaver import get_datetime_now
from tools.generic.panda.read_sql import read_sql
from tools.generic.panda.sql import create_select
from tools.generic.idweaver import upperfy_id
from tools.generic.idweaver import lowerfy_id
import cx_Oracle


def get_workstation_name_and_ids(conf_database):
    conf = conf_database['oracle']
    dsn = cx_Oracle.makedsn(
        conf['hostname'],
        conf['portnumber'],
        service_name=conf['servicename']
    )
    try:
        cx_Oracle.init_oracle_client(lib_dir='./instantclient_19_9')
    except cx_Oracle.DatabaseError:
        pass
    conn = cx_Oracle.Connection(user='TNT_CONFIGURATION', password=conf['psw'], dsn=dsn)      
    exportdate = get_datetime_now()
    query = "SELECT NAME, ID FROM TNT_CONFIGURATION.WORKSTATION"
    data = read_sql(query, conn)
    final_workstation_ids = {}
    for index, row in data.iterrows():
        final_workstation_ids[row['NAME']] = lowerfy_id(row['ID'])
    return final_workstation_ids
