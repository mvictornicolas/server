import pandas as pd
from uuid import UUID

def read_sql(query:str, connection, convertuuid=True, uuidformat='upper'):
    data = pd.read_sql(query, connection)
    if convertuuid == True:
        for columnName, columnData in data.iteritems():
            if uuidformat == 'upper':
                try:
                    new_data = data[columnName].apply(lambda u: str(UUID(bytes=u)).replace('-','').upper())
                except (ValueError, TypeError):
                    continue
                data[columnName] = new_data
            if uuidformat == 'lower':
                try:
                    new_data = data[columnName].apply(lambda u: str(UUID(bytes=u)))
                except (ValueError, TypeError):
                    continue
                data[columnName] = new_data
    return data
