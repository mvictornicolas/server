import cx_Oracle

NEW_DIR = 'C:\\Users\\Usuário\\Documents\\DEVELOPMENT\\New Aessia and ASTools\\astools server\\instantclient_19_9'
OLD_DIR = './instantclient_19_9'

def oracle_connect(conf_database):
    cx_Oracle.init_oracle_client(lib_dir=OLD_DIR)
    conf_database = conf_database['oracle']
    dsn = cx_Oracle.makedsn(
        conf_database['hostname'], 
        conf_database['portnumber'], 
        service_name=conf_database['servicename']
    )

    conn = cx_Oracle.connect(user=conf_database['username'], password=conf_database['psw'], dsn=dsn)
    return conn
