import uuid
def upperfy_id(uuid, dehyphenize=True):
    if dehyphenize == True:
        return uuid.upper().replace('-','')
    if dehyphenize == False:
        return uuid.upper()

def lowerfy_id(uuid, hyphenize=True):
    if hyphenize == True:
        uuid = uuid[:8]+'-'+uuid[8:12]+'-'+uuid[12:16]+'-'+uuid[16:20]+'-'+uuid[20:]
        return uuid.lower()
