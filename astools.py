from flask import Flask
# from management.repository import db
from blueprints.apifreshdesk import freshdesk
from blueprints.apifourelements import fourelements
from blueprints.apigeneric import generic

flask_host = '0.0.0.0'
flask_port = 5002

username = "root"
psw = "as2Group"
host = "192.168.15.201"
port = 3306
database = "iot-core"

app = Flask(__name__)
app.register_blueprint(freshdesk)
app.register_blueprint(fourelements)
app.register_blueprint(generic)
# app.register_blueprint(sensor)
# app.register_blueprint(device)
# app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://{username}:{psw}@{host}:{port}/{database}'
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# db.init_app(app)

# with app.app_context():
#    db.create_all()

if __name__ == "__main__":
    flaskserver = app.run(debug=True, host=flask_host, port=flask_port)
