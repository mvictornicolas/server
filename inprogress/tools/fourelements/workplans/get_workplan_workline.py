from tools.connectors.oracle import oracle_connect
from tools.timeweaver import get_datetime_now
from tools.panda.read_sql import read_sql
from tools.panda.sql import create_select
from tools.idweaver import upperfy_id
def get_workline_by_workplan_id(workplan_id, conf_database):
    conn = oracle_connect(conf_database)
    exportdate = get_datetime_now()
    query = create_select('TNT_OPERATIONAL.WORKPLAN', 'ID', upperfy_id(workplan_id))
    data = read_sql(query, conn)
    workstation_id = data['WORKSTATION_ID'][0]
    print(workstation_id)
    query = create_select('TNT_CONFIGURATION.WORKSTATION', 'ID', workstation_id)
    data = read_sql(query, conn)
    workline_id = data['WORKLINE_ID'][0]
    return workline_id