import requests, json
def get_auth(url):
    user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'
    cookie = 'JSESSIONID=279C0FC3A8C4604744B2BC813E633B5B'
    payload="{\"username\":\"aessia\",\"password\":\"aessia\",\"client_id\":\"tnt\",\"grant_type\":\"password\"}"
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'User-Agent': user_agent,
        'Content-Type': 'application/json',
        'Cookie': cookie
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    bearer = json.loads(response.text)['access_token']
    headers['Authorization'] = f'Bearer {bearer}'
    return headers