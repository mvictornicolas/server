import datetime
def get_datetime_now():
    return datetime.datetime.now().strftime("%d-%m-%Y %H-%M-%S")