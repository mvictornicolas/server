import pandas as pd

def to_query(data, pandas=True):
    if pandas == True:
        column_id_list = data.tolist()
        counting = -1
        for item in column_id_list:
            item = "'"+item+"',"
            counting += 1
            if counting == len(column_id_list):
                item = "'"+item+"'"
        return column_id_list
    elif pandas == False:
        counting = -1
        for item in data:
            item = "'"+item+"',"
            counting += 1
            if counting == len(data):
                item = "'"+item+"'"
        return data