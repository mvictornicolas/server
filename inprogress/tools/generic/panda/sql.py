import pandas as pd

def create_select(table, field, value, listit=False, final=""):
    query = f"SELECT * FROM {str(table)} WHERE {str(field)} = '{str(value)}' {final}"
    return query

def create_insert(data:pd.DataFrame, table, fields:list, values:list):
    sql_commands = ""
    for index, row in data.iterrows():
        fields_string = values_string = ""
        counting = -1
        for f in fields:
            if counting == len(f):
                fields_string = fields_string + f
            else:
                fields_string = fields_string + f + ', '
                counting+=1
        print(fields_string)
        counting = -1
        for v in values:
            if counting == len(v):
                values_string = values_string + f"'{row[v]}'"
            else:
                values_string = values_string + f"'{row[v]}', "
        print(values_string)
        insert_string = f"INSERT INTO {table}({fields_string}) VALUES({values_string});"
        sql_commands = sql_commands + insert_string + "\n"
    return sql_commands