import json
import pandas as pd
from tools.generic.connectors import oracle
from tools.generic.timeweaver import get_datetime_now
from tools.generic.panda.read_sql import read_sql
from tools.generic.panda.sql import create_select

conf_database = {}
conf_database['hostname'] = str('172.17.1.12'), 
conf_database['portnumber'] = str('1521'), 
conf_database['servicename'] = str('spdb'),
conf_database['username'] = str("TNT_DEVICE"),
conf_database['psw'] = str("mwm_1nt3rn4c10n4l")
conn = oracle.oracle_connect(conf_database)
query = "SELECT * FROM TNT_COMMONS_MWM.MAN_PRODUCTION_CALENDAR ORDER BY DATE_PLANNING ASC"
data = read_sql(query, conn)
xl_data = pd.read_excel('calendar.xlsx', index_col=None, header=None)
print(data)
print('-'*20)
print(xl_data)
