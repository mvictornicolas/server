from flask import Blueprint, request
from json import loads
from re import sub
import validators.generic as validators
from scripts.freshdesk.createticket import create_ticket as tkt_create
from scripts.freshdesk.noteticket import note_ticket as tkt_note
from scripts.freshdesk.replyticket import reply_ticket as tkt_reply
from scripts.freshdesk.updateticket import update_ticket as tkt_update
#import management.repository as repo
#from management.sensor.services import create_sgroup, create_sensor, update_sgroup, update_sensor, read_sgroups, read_sgroup, read_sensors, read_sensor, delete_sgroup, delete_sensor

freshdesk = Blueprint('freshdesk', __name__)


@freshdesk.route("/desk/help", methods=['GET'])
def get_help():
    return "help"

@freshdesk.route("/desk/create", methods=['POST'])
def create_ticket():
    data = request.json
    print(data)
    fields = [
        'email', 'group_id', 'subject', 'description', 'source', 
        'priority','status', 'responder_id','type',
    ]
    validated = validators.validate_json(data, fields)['validatedData']
    print(validated)
    if validated['type'].lower() == "problema":
        validated['type'] = "Tenho um problema"
    elif validated['type'].lower() == "solicitacao":
        validated['type'] = "Solicitação"
    elif validated['type'].lower() == "funcionalidade":
        validated['type'] = "Gostaria de uma nova funcionalidade"
    elif validated['type'].lower() == "enhancement":
        validated['type'] = "Small Enhancement"
    elif validated['type'].lower() == "duvida":
        validated['type'] = "Uma dúvida"
    response = tkt_create(**validated)
    fields_to_response = ""
    for fr in validated:
        fields_to_response = fields_to_response + f"{str(fr)}: {str(validated[fr])}, "
    response = loads(response.text)
    print(response)
    response["aessia"] = f"O ticket {response['id']} com as informaçoes {fields_to_response}foi criado com sucesso"
    return response

@freshdesk.route("/desk/update", methods=['POST'])
def update_ticket():
    data = request.json
    ticket_id = data['ticketId']
    fields = ['subject', 'type', 'status', 'priority', 'description', 'attachments']
    validated = validators.validate_json(data, fields)['validatedData']
    for v in validated:
        validated[v] = (None, validated[v])
    response = tkt_update(ticket_id=ticket_id, data=validated)
    print(validated)
    fields_to_response = ""
    for fr in validated:
        fields_to_response = fields_to_response + f"{str(fr)}: {str(validated[fr][1])}, "
    response = loads(response.text)
    response["aessia"] = f"O ticket {ticket_id} teve as informacoes atualizadas para {fields_to_response}com sucesso"
    return response

@freshdesk.route("/desk/update/<ticket_id>", methods=['POST'])
def update_ticket_with_id(ticket_id):
    data = request.json
    fields = ['subject', 'type', 'priority', 'description', 'attachments']
    validated = validators.validate_json(data, fields)['validatedData']
    for v in validated:
        validated[v] = (None, validated[v])
    response = tkt_update(ticket_id=ticket_id, data=validated)
    print(validated)
    fields_to_response = ""
    for fr in validated:
        fields_to_response = fields_to_response + f"{str(fr)}: {str(validated[fr][1])}, "
    response = loads(response.text)
    response["aessia"] = f"O ticket {ticket_id} teve as informacoes atualizadas para {fields_to_response}com sucesso"
    return response

@freshdesk.route("/desk/update/status", methods=['POST'])
def change_status():
    data = request.json
    ticket_id = data['ticketId']
    acceptable = {
        "end":"4", "pend":"3", "queue":"10", "progress":"8", "report":"11", "close":"5",
        "#end":"4", "#pend":"3", "#queue":"10", "#progress":"8", "#report":"11", "#close":"5",
    }
    response = tkt_update(ticket_id=ticket_id, data={"status": (None, acceptable[data['key']])})
    response = loads(response.text)
    response["aessia"] = f'O ticket {ticket_id} foi atualizado para o status de {data["key"]}[{acceptable[data["key"]]}]'
    return response

@freshdesk.route("/desk/update/status/<ticket_id>", methods=['POST'])
def change_status_with_id(ticket_id):
    data = request.json
    acceptable = {
        "end":"4", "pend":"3", "queue":"10", "progress":"8", "report":"11", "close":"5",
        "#end":"4", "#pend":"3", "#queue":"10", "#progress":"8", "#report":"11", "#close":"5",
    }
    response = tkt_update(ticket_id=ticket_id, data={"status": (None, acceptable[data['key']])}).text
    response = loads(response.text)
    response["aessia"] = f'O ticket {ticket_id} foi atualizado para o status de {data["key"]}[{acceptable[data["key"]]}]'
    return response

@freshdesk.route("/desk/note", methods=['POST'])
def create_note():
    data = request.json
    response = tkt_note(ticket_id=data['ticketId'], body=data['body'], private=data['private'])
    response = loads(response.text)
    response["aessia"] = f"O ticket {data['ticketId']} foi acrescido da nota {data['body'][:14]}[...] com sucesso"
    return response

@freshdesk.route("/desk/note/<ticket_id>", methods=['POST'])
def create_note_with_id(ticket_id):
    data = request.json
    response = tkt_note(ticket_id=ticket_id, body=data['body'], private=data['private'])
    response = loads(response.text)
    response["aessia"] = f"O ticket {ticket_id} foi acrescido da nota {data['body'][:14]}[...] com sucesso"
    return response

@freshdesk.route("/desk/reply", methods=['POST'])
def create_reply():
    data = request.json
    response = tkt_reply(ticket_id=data['ticketId'], body=data['body'])
    response = loads(response.text)
    response["aessia"] = f"O ticket {data['ticketId']} foi acrescido da resposta {data['body'][:14]}[...] com sucesso"
    return response

@freshdesk.route("/desk/reply/<ticket_id>", methods=['POST'])
def create_reply_with_id(ticket_id):
    data = request.json
    response = tkt_reply(ticket_id=ticket_id, body=data['body'])
    response = loads(response.text)
    response["aessia"] = f"O ticket {ticket_id} foi acrescido da resposta {data['body'][:14]}[...] com sucesso"
    return response
