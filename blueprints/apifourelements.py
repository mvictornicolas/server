import json
from flask import Blueprint, request, send_from_directory
from tools.fourelements.report.rep_manuf_result import manufacturing_result_report

from tools.fourelements.worklines.get_workline_by_name import get_workline_id_by_name
from scripts.fourelements.keycloak import create_keycloak_user, get_keycloak_token
from scripts.fourelements.devices import request_devices
from scripts.fourelements.redismanager import redis_manage
from scripts.fourelements.testintegration import test_integration
from scripts.fourelements.remotepowershell import view_ram_modules_usage
from scripts.fourelements.fronttestcomponent import workplan_component_not_found_error
# import management.repository as repo
# from management.sensor.services import create_sgroup, create_sensor, update_sgroup, update_sensor, read_sgroups, read_sgroup, read_sensors, read_sensor, delete_sgroup, delete_sensor

fourelements = Blueprint('fourelements', __name__)
# server_ip = "172.17.1.192"
servers = {
    'prod': '172.17.1.192',
    'qa': '172.17.1.198'
}


@fourelements.route("/4elements/info", methods=['GET'])
def get_info():
    return (request)


@fourelements.route("/4elements/redis/workstations", methods=['POST'])
def redis_workstations():
    data = request.json
    response = redis_manage(data, data['server'])
    return response


@fourelements.route("/4elements/integration/test", methods=['GET'])
def integration_test():
    data = request.json
    server_ip = servers[data['server']]
    response = test_integration(server_ip)
    return response


@fourelements.route("/4elements/server/status", methods=['GET'])
def server_status():
    data = request.json
    server_ip = servers[data['server']]
    response = view_ram_modules_usage(server_ip)
    response = {"aessia": response}
    return response


@fourelements.route("/4elements/workplan/componenterror", methods=['POST'])
def component_error():
    data = request.json
    print(data)
    server_ip = servers[data['server']]
    data['hostname'] = server_ip
    response = workplan_component_not_found_error(data)
    return response


@fourelements.route("/4elements/devices", methods=['GET'])
def get_devices():
    data = request.json
    server_ip = servers[data['server']]
    data = request_devices(server_ip, 8050).json()
    new_data = {}
    c = -1
    for d in data:
        c += 1
        device = {}
        device['name'] = d['description']
        device['status'] = d['status']
        new_data[d['tag']] = device
    return new_data


@fourelements.route("/4elements/keycloak/register", methods=["POST"])
def create_user():
    data = request.json
    data['badge']
    data['role']
    data['firstName']
    data['lastName']
    data['server']
    conf_database = json.load(open("./configuration/database.json"))
    workline_ids = []
    for workline in data['worklines']:
        workline_id = get_workline_id_by_name(workline, data['server'], conf_database)
        workline_ids.append(workline_id)
    data['worklines'] = workline_ids
    token = get_keycloak_token(data)
    result = create_keycloak_user(data, token)
    return result


@fourelements.route("/4elements/report", methods=["POST"])
def get_report():
    data = request.json
    if data['OS'].lower() == 'windows':
        folder = "instantclient_21_1"
    elif data['OS'].lower() == 'linux':
        folder = "instantclient_19_9"
    if data['server'].lower() == 'prod':
        hostname = '172.17.1.12'
        portnumber = '1521'
        servicename = 'spdb'
        user_name = 'TNT_OPERATIONAL'
        pw = 'mwm_1nt3rn4c10n4l'
    elif data['server'].lower() == 'qa':
        hostname = '172.17.1.12'
        portnumber = '1525'
        servicename = 'sphm'
        user_name = 'TNT_OPERATIONAL'
        pw = 'as2Group'
    else:
        return {"error": "Select prod or qa server"}
    start_date = data['startDate']
    end_date = data['endDate']
    operation_number = data['operationNumber']
    data_type = data['dataType'].upper()
    if data_type not in ['PLAIN_DATA', 'EFFECTIVE_DATA']:
        return {"error": "dataType needs to be PLAIN_DATA or EFFECTIVE_DATA"}
    get_serial_number = data['getSerialNumber']
    if get_serial_number not in [True, False]:
        return {"error": "getSerialNumber needs to be a boolean!"}
    json_fields = data['jsonFields']
    result_paths = manufacturing_result_report(
        hostname, portnumber, servicename, user_name, pw, folder,
        start_date, end_date, operation_number, data_type, get_serial_number, json_fields
        )
    try:
        if result_paths['error']:
            return result_paths
    except Exception:
        pass
    return send_from_directory(result_paths[0], result_paths[1])
