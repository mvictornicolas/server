from flask import Blueprint, request

#import management.repository as repo
#from management.sensor.services import create_sgroup, create_sensor, update_sgroup, update_sensor, read_sgroups, read_sgroup, read_sensors, read_sensor, delete_sgroup, delete_sensor

generic = Blueprint('generic', __name__)

@generic.route("/generic/help", methods=['GET'])
def get_info():
    return {"aessia":{"user":"Aessia help example", "group":"Aessia group help example"}}
    